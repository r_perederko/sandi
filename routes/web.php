<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::get('/import-products', 'ProductController@import')->name('import_products');


Route::group(['prefix' => '{locale}', 'as' => 'localized.', 'where' => ['locale' => 'ru|uk'], 'middleware' => 'setlocale'], function () {
    Route::get('/welcome', function () {
        return view('welcome');
    })->name('welcome');

    Route::get('/product/{id}', 'ProductController@show')->name('product');

    Route::get('/products', 'ProductController@index')->name('products');

    Route::get('/', function () {
        return view('home');
    })->name('home');


});