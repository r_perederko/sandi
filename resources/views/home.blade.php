@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <form method="GET" action="{{ route('import_products') }}">
                            <button id="import" type="submit" name="source" value="sandi"
                                    class="btn btn-primary btn-lg"><i class='fa fa-spinner fa-spin'></i>Start import
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection