@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Products</div>
                    <div class="card-body">
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @foreach ($models as $model)
                                <li class="nav-item">
                                    <a class="nav-link"
                                       href="{{ route('localized.product', [app()->getLocale(), $model->id]) }}">{{ $model->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                        {{ $models->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection