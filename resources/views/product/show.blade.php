@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$model->name}} <b class="">( @money($model->price, 'UAH') )</b></div>

                    <div class="card-body">
                        <p>{{$model->description}}</p>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($model->characteristics as $characteristic)
                                <tr>
                                    <td>
                                        {{ $characteristic->name }}
                                    </td>
                                    <td>
                                        {{ $characteristic->value }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection