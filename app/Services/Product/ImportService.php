<?php

namespace App\Services\Product;


use App\Product;
use App\Services\Product\Contracts\ProductsImportInterface;
use App\Services\Product\Contracts\ProductsSourceInterface;
use App\Services\Product\Dto\ProductCharacteristic;
use Illuminate\Support\Facades\DB;

class ImportService implements ProductsImportInterface
{
    public function process(ProductsSourceInterface $source)
    {
        $products = $source->getProducts();

        foreach ($products as $productDto) {
            ini_set('max_execution_time', 60);

            DB::transaction(function () use ($productDto) {
                /** @var Product $product */
                $product = Product::firstOrNew([
                    'sku' => $productDto->sku,
                ]);

                /** @var \App\Services\Product\Dto\Product $productDto */
                $translatesArray = $productDto->getTranslates()->toTranslatesArray();

                $product->fill([
                        'price' => $productDto->price,
                    ] + $translatesArray);

                $product->save();

                $product->characteristics()->delete();

                $characteristics = [];
                foreach ($productDto->getCharacteristics() as $characteristicDto) {
                    /** @var ProductCharacteristic $characteristicDto */
                    $characteristics[] = new \App\ProductCharacteristic($characteristicDto->getTranslates()->toTranslatesArray());
                }

                $product->characteristics()->saveMany($characteristics);
            });
        }
    }
}