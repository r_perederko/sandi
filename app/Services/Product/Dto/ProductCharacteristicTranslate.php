<?php

namespace App\Services\Product\Dto;


class ProductCharacteristicTranslate
{
    /** @var string */
    public $language;
    /** @var string */
    public $name;
    /** @var string */
    public $value;

    public function __construct(string $language, string $name, string $value)
    {
        $this->language = $language;
        $this->name = $name;
        $this->value = $value;
    }
}