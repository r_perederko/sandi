<?php

namespace App\Services\Product\Dto;


use Gamez\Illuminate\Support\TypedCollection;

class ProductTranslatesCollection extends TypedCollection
{
    protected static $allowedTypes = [ProductTranslation::class];

    public function toTranslatesArray(): array
    {
        $result = [];

        $this->each(function (ProductTranslation $translation) use (&$result) {
            $result[$translation->language] = [
                'name' => $translation->name,
                'description' => $translation->description,
            ];
        });

        return $result;
    }
}