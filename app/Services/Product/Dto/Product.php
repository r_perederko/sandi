<?php

namespace App\Services\Product\Dto;


class Product
{
    /** @var string */
    public $sku;
    /** @var float */
    public $price;
    /** @var ProductTranslatesCollection */
    protected $translates;
    /** @var ProductCharacteristicsCollection */
    protected $characteristics;

    /**
     * @return mixed
     */
    public function getTranslates(): ProductTranslatesCollection
    {
        return $this->translates;
    }

    /**
     * @param ProductTranslatesCollection $translates
     * @return Product
     */
    public function setTranslates(ProductTranslatesCollection $translates): self
    {
        $this->translates = $translates;

        return $this;
    }

    /**
     * @return ProductCharacteristicsCollection
     */
    public function getCharacteristics(): ProductCharacteristicsCollection
    {
        return $this->characteristics;
    }

    /**
     * @param ProductCharacteristicsCollection $characteristics
     * @return Product
     */
    public function setCharacteristics(ProductCharacteristicsCollection $characteristics): self
    {
        $this->characteristics = $characteristics;

        return $this;
    }
}