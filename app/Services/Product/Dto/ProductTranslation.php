<?php

namespace App\Services\Product\Dto;


class ProductTranslation
{
    /** @var string */
    public $language;
    /** @var string */
    public $name;
    /** @var string */
    public $description;

    public function __construct(string $language, string $name, string $description)
    {
        $this->language = $language;
        $this->name = $name;
        $this->description = $description;
    }
}