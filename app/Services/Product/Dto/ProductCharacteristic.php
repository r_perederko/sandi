<?php

namespace App\Services\Product\Dto;


class ProductCharacteristic
{
    protected $translates;

    public function __construct(ProductCharacteristicsTranslatesCollection $translates)
    {
        $this->translates = $translates;
    }

    public function getTranslates(): ProductCharacteristicsTranslatesCollection
    {
        return $this->translates;
    }
}