<?php

namespace App\Services\Product\Dto;


use Gamez\Illuminate\Support\TypedCollection;

class ProductCharacteristicsCollection extends TypedCollection
{
    protected static $allowedTypes = [ProductCharacteristic::class];
}