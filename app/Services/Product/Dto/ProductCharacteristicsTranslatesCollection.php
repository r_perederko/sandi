<?php

namespace App\Services\Product\Dto;


use Gamez\Illuminate\Support\TypedCollection;

class ProductCharacteristicsTranslatesCollection extends TypedCollection
{
    protected static $allowedTypes = [ProductCharacteristicTranslate::class];

    public function toTranslatesArray(): array
    {
        $result = [];

        $this->each(function (ProductCharacteristicTranslate $translation) use (&$result) {
            $result[$translation->language] = [
                'name' => $translation->name,
                'value' => $translation->value,
            ];
        });

        return $result;
    }
}