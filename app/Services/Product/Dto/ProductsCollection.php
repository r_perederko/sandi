<?php

namespace App\Services\Product\Dto;


use Gamez\Illuminate\Support\TypedCollection;

class ProductsCollection extends TypedCollection
{
    protected static $allowedTypes = [Product::class];
}