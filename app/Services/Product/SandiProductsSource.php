<?php

namespace App\Services\Product;


use App\Services\Product\Contracts\ProductsSourceInterface;
use App\Services\Product\Dto\Product;
use App\Services\Product\Dto\ProductCharacteristic;
use App\Services\Product\Dto\ProductCharacteristicsCollection;
use App\Services\Product\Dto\ProductCharacteristicsTranslatesCollection;
use App\Services\Product\Dto\ProductCharacteristicTranslate;
use App\Services\Product\Dto\ProductsCollection;
use App\Services\Product\Dto\ProductTranslatesCollection;
use App\Services\Product\Dto\ProductTranslation;
use GuzzleHttp\Client;

class SandiProductsSource implements ProductsSourceInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * SandiProductsSource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getData(): array
    {
        $response = $this->client->request('get', 'https://b2b-sandi.com.ua/api/products');

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @return ProductsCollection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProducts(): ProductsCollection
    {
        $products = new ProductsCollection();

        foreach ($this->getData() as $rawProduct) {
            $products->add($this->createProduct($rawProduct));
        }

        return $products;
    }

    /**
     * @param array $rawData
     * @return Product
     */
    protected function createProduct(array $rawData): Product
    {
        $product = new Product();
        $product->sku = $rawData['sku'];
        $product->price = $rawData['price'];
        $product->setTranslates($this->createProductTranslations($rawData))
            ->setCharacteristics($this->createProductCharacteristics($rawData['characteristics']));

        return $product;
    }

    /**
     * @param array $rawData
     * @return ProductTranslatesCollection
     */
    protected function createProductTranslations(array $rawData): ProductTranslatesCollection
    {
        $productTranslates = new ProductTranslatesCollection();

        $productTranslates->add(new ProductTranslation('ru', $rawData['name_ru'], $rawData['description_ru']));
        $productTranslates->add(new ProductTranslation('uk', $rawData['name_uk'], $rawData['description_uk']));

        return $productTranslates;
    }

    /**
     * @param array $rawData
     * @return ProductCharacteristicsCollection
     */
    protected function createProductCharacteristics(array $rawData): ProductCharacteristicsCollection
    {
        $characteristics = new ProductCharacteristicsCollection();

        foreach ($rawData as $characteristic) {
            $characteristicTranslates = new ProductCharacteristicsTranslatesCollection();
            $characteristicTranslates->add(new ProductCharacteristicTranslate('ru', $characteristic['name_ru'], $characteristic['value_ru']));
            $characteristicTranslates->add(new ProductCharacteristicTranslate('uk', $characteristic['name_uk'], $characteristic['value_uk']));
            $characteristics->add(new ProductCharacteristic($characteristicTranslates));
        }

        return $characteristics;
    }
}