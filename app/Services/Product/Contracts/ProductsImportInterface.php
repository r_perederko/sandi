<?php

namespace App\Services\Product\Contracts;


interface ProductsImportInterface
{
    public function process(ProductsSourceInterface $source);
}