<?php

namespace App\Services\Product\Contracts;


use App\Services\Product\Dto\ProductsCollection;

interface ProductsSourceInterface
{
    public function getProducts(): ProductsCollection;
}