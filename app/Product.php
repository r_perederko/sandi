<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name', 'description'];

    public $fillable = ['sku', 'price'];

    public function characteristics()
    {
        return $this->hasMany(ProductCharacteristic::class);
    }

    public function getPriceAttribute($price)
    {
        return $price * 100;
    }
}
