<?php

namespace App;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProductCharacteristic extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name', 'value'];
}
