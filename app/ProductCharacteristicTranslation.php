<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCharacteristicTranslation extends Model
{
    protected $fillable = ['name', 'value'];
    public $timestamps = false;
}
