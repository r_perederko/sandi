<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsImportRequest;
use App\Product;
use App\Services\Product\ImportService;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(15);

        return view('product.index', ['models' => $products]);
    }

    public function show($locale, $id)
    {
        return view('product.show', ['model' => Product::findOrFail(['id' => $id])->first()]);
    }

    /**
     * @param ImportService $importService
     * @param ProductsImportRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function import(ImportService $importService, ProductsImportRequest $request)
    {
        $importService->process($request->getProductsSource());

        return redirect(route('localized.products', app()->getLocale()));
    }
}
