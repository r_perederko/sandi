<?php

namespace App\Http\Requests;

use App\Services\Product\Contracts\ProductsSourceInterface;
use App\Services\Product\SandiProductsSource;
use http\Exception\InvalidArgumentException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ProductsImportRequest
 * @package App\Http\Requests
 *
 * @property-read string $source
 */
class ProductsImportRequest extends FormRequest
{
    const SANDI = 'sandi';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'source' => ['required', Rule::in([static::SANDI])],
        ];
    }

    /**
     * @return ProductsSourceInterface
     */
    public function getProductsSource(): ProductsSourceInterface
    {
        switch ($this->source) {
            case static::SANDI:
                $source = SandiProductsSource::class;
                break;
            default:
                throw new InvalidArgumentException();
        }

        return app()->get($source);
    }
}
