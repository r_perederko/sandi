<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCharacteristicTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_characteristic_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();

            // Foreign key to the main model
            $table->unsignedBigInteger('product_characteristic_id');
            $table->unique(['product_characteristic_id', 'locale'], 'pct_uk');
            $table->foreign('product_characteristic_id','pct_fk1')
                ->references('id')
                ->on('product_characteristics')
                ->onDelete('cascade');

            // Actual fields you want to translate
            $table->string('name');
            $table->longText('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_characteristic_translations');
    }
}
